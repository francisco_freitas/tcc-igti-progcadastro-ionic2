import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { AddItemPage } from './add-item';

@NgModule({
  declarations: [
    AddItemPage,
  ],
  imports: [
    IonicModule.forRoot(AddItemPage),
  ],
  exports: [
    AddItemPage
  ]
})
export class AddItemModule {}
