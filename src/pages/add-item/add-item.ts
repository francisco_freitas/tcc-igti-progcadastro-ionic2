import { Pessoa } from './../../app/model/pessoa.model';
import { Component } from '@angular/core';
import { NavController, ViewController } from 'ionic-angular';

@Component({
  selector: 'page-add-item',
  templateUrl: 'add-item.html'
})
export class AddItemPage {

pessoa: Pessoa = new Pessoa();

  constructor(public navCtrl: NavController, public view: ViewController) {

  }

  saveItem(){
    this.view.dismiss(this.pessoa);
  }

  close(){
    this.view.dismiss();
  }

}
