import { Pessoa } from './../../app/model/pessoa.model';
import { Component } from '@angular/core';
import { ModalController, NavController } from 'ionic-angular';
import { AddItemPage } from '../add-item/add-item'
import { ItemDetailPage } from '../item-detail/item-detail';
import { Data } from '../../providers/data';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  public items: Pessoa[] = [];

  constructor(public navCtrl: NavController, public modalCtrl: ModalController, public dataService: Data) {

    this.dataService.getPessoas().then((data) => this.items = data);

  }

  ionViewDidLoad() {

    this.dataService.getPessoas().then((data) => this.items = data);

  }

  ionViewWillEnter() {

    this.dataService.getPessoas().then((data) => this.items = data);
  }

  addItem() {

    let addModal = this.modalCtrl.create(AddItemPage);

    addModal.onDidDismiss((item) => {
      if (item) {
        this.saveItem(item);
      }

    });

    addModal.present();

  }

  saveItem(item) {
    this.items.push(item);
    this.dataService.addPessoa(item);
  }

  viewItem(item) {
    this.navCtrl.push(ItemDetailPage, {
      pessoa: item
    });
  }

}
