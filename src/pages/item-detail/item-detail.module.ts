import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { ItemDetailPage } from './item-detail';

@NgModule({
  declarations: [
    ItemDetailPage,
  ],
  imports: [
    IonicModule.forRoot(ItemDetailPage),
  ],
  exports: [
    ItemDetailPage
  ]
})
export class ItemDetailModule {}
