import { Data } from './../../providers/data';
import { Pessoa } from './../../app/model/pessoa.model';
import { Component, OnInit } from '@angular/core';
import { NavParams, NavController } from 'ionic-angular';

@Component({
  selector: 'page-item-detail',
  templateUrl: 'item-detail.html'
})
export class ItemDetailPage implements OnInit {
  title;
  pessoa: Pessoa;

  constructor(public navParams: NavParams, private dataService: Data, private navController: NavController){

  }

ngOnInit() {
    this.pessoa = this.navParams.get('pessoa');
    this.title = this.pessoa.nome;
}

salvar(){
  this.dataService.updatePessoa(this.pessoa);
  this.navController.pop();
}

RemoveItem(){
  this.dataService.deletePessoa(this.pessoa.id);
  this.navController.pop();
}

  ionViewDidLoad() {

  }

}
