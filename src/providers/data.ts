import { Pessoa } from './../app/model/pessoa.model';
import { Storage } from '@ionic/storage';
import { Injectable } from '@angular/core';
import uuidV4 from 'uuid/v4';
import Dexie from 'Dexie';

@Injectable()
export class Data extends Dexie{

    pessoas: Dexie.Table<Pessoa, string>;

  constructor(public storage: Storage){
        //DEXIE - Nome do banco
         super('pessoaDB');
        //Mapeia somente os campos que devem ser indexados
        //o primeiro campo é sempre a chave primaria
        this.version(1).stores({
            pessoas: 'id,cpf '
         });
  }

  // getData() {
  //   this.storage.get('pessoas').then((data)=>this.listaPessoas = data);
  // }

  // addSave(data){
  //   data.id = uuidV4();
  //   let newData = JSON.stringify(data);
  //   this.storage.set(`pessoas`, newData);
  // }

  // Save(data){
  //   let newData = JSON.stringify(data);
  //   this.storage.set(`pessoas`, this.listaPessoas);
  // }

  // remove(data){

  //   this.storage.remove(`pessoas${data.id}`);
  // }

    getPessoa(expenseId: string): Dexie.Promise<Pessoa> {
        return this.pessoas.get(expenseId);
        //Cria copia do objeto para evitarreferencia em memoria, dando a opcao de confirmar alteracoes
        // const expense = this.expenses.find(expense => expense.id === expenseId);
        // return Object.assign({}, expense);
    }
    addPessoa(pessoa: Pessoa) {
        pessoa.id = uuidV4();
        this.pessoas.add(pessoa);
    }
    deletePessoa(pessoaId: string) {
        this.pessoas.delete(pessoaId);
    }
    updatePessoa(pessoa: Pessoa) {
        this.pessoas.update(pessoa.id, pessoa);
    }
    getPessoas(): Dexie.Promise<Pessoa[]> {
        return this.pessoas.toArray();
    }

}
